import React, { Component } from "react";
import { Link } from "react-router-dom";

//  material ui
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import FormHelperText from "@material-ui/core/FormHelperText";

import { fetchBook, fetchGenres, addBook, updateBook } from "../api/api";

const dateFormat = require("dateformat");

const styles = {
  formControl: {
    margin: 3,
    minWidth: 200
  },
  formWrapper: {
    margin: 20,
    padding: 25
  }
};

class BookForm extends Component {
  state = {
    GenreId: "",
    author: "",
    description: "",
    ISBN: "",
    price: "",
    publicationDate: "",
    title: "",
    isInEditMode: false,
    genres: [],
    errors: {
      GenreId: "",
      author: "",
      description: "",
      price: "",
      publicationDate: "",
      ISBN: "",
      title: ""
    }
  };

  componentDidMount() {
    const { params } = this.props.match;

    this.setGenres();

    if (params && params.id) {
      this.setState({ headline: "View Book", isInEditMode: true });
      this.setBook(params.id);
    } else {
      this.setState({ headline: "New Book" });
    }
  }

  handleValidate = fieldName => {
    const { errors } = this.state;

    let error = "";

    switch (fieldName) {
      case "title":
        if (!this.state[fieldName]) {
          error = "Title cannot be empty";
        }
        break;
      case "author":
        if (!this.state[fieldName]) {
          error = "Author cannot be empty";
        }
        break;
      case "description":
        if (!this.state[fieldName]) {
          error = "Description cannot be empty";
        }
        break;
      case "price":
        if (!this.state[fieldName]) {
          error = "Price cannot be empty";
        } else if (isNaN(this.state[fieldName])) {
          error = "Price must be a number";
        } else if (Number(this.state[fieldName]) < 0) {
          error = "Price must be a positive number";
        }
        break;
      case "publicationDate":
        if (!this.state[fieldName] || !new Date(this.state[fieldName])) {
          error = "Publication date must be a valid date";
        }
        break;
      case "ISBN":
        if (!this.state[fieldName]) {
          error = "ISBN cannot be empty";
        } else if (isNaN(this.state[fieldName])) {
          error = "ISBN must be a number";
        } else if (Number(this.state[fieldName]) < 1000000000) {
          error = "ISBN must have a least 10 digits";
        }
        break;
      case "GenreId":
        if (!this.state[fieldName]) {
          error = "Genre cannot be empty";
        }
        break;
      default:
        break;
    }

    this.setState({
      errors: {
        ...errors,
        [fieldName]: error
      }
    });

    if (error.length > 0) {
      return false;
    } else {
      return true;
    }
  };

  validateAllFields = async () => {
    const { errors } = this.state;
    let isAllValid = true;

    const fieldNames = Object.keys(errors);

    for (const fieldName of fieldNames) {
      let isFieldValid = await this.handleValidate(fieldName);
      isAllValid = isFieldValid && isAllValid;
    }

    return isAllValid;
  };

  setGenres = () => {
    fetchGenres().then(genres => {
      this.setState({ genres });
    });
  };

  setBook = bookId => {
    fetchBook(bookId)
      .then(book => {
        book.publicationDate = dateFormat(book.publicationDate, "yyyy-mm-dd");

        this.setState({ ...book });
      })
      .catch(err => {
        console.log(err);
        this.props.history.push("/books/");
      });
  };

  renderGenreOptions = () => {
    return this.state.genres.map(genre => {
      return (
        <MenuItem value={genre.id} key={genre.id}>
          {genre.name}
        </MenuItem>
      );
    });
  };

  onSubmit = async () => {
    const isValid = await this.validateAllFields();
    let method;

    if (isValid) {
        if (this.state.isInEditMode) {
            method = updateBook;
        } else {
            method = addBook;
        }
        method({ ...this.state })
        .then(book => {
          if (book) {
            this.props.history.push("/books/");
          }
        })
        .catch(err => {
          this.props.history.push("/error/");
        });
    }
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <Card style={styles.formWrapper}>
        <CardContent>
          <Typography variant="h4" gutterBottom>
            {this.state.headline}
          </Typography>
          <form onSubmit={this.onSubmit}>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <TextField
                  required
                  value={this.state.title}
                  onChange={this.handleChange}
                  id="title"
                  name="title"
                  label="Title"
                  fullWidth
                  helperText={this.state.errors.title}
                  error={this.state.errors.title ? true : false}
                  onBlur={() => this.handleValidate("title")}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  value={this.state.author}
                  onChange={this.handleChange}
                  id="author"
                  name="author"
                  label="Author"
                  fullWidth
                  helperText={this.state.errors.author}
                  error={this.state.errors.author ? true : false}
                  onBlur={() => this.handleValidate("author")}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  value={this.state.description}
                  onChange={this.handleChange}
                  id="description"
                  name="description"
                  label="Description"
                  multiline
                  fullWidth
                  helperText={this.state.errors.description}
                  error={this.state.errors.description ? true : false}
                  onBlur={() => this.handleValidate("description")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  value={this.state.price}
                  onChange={this.handleChange}
                  id="price"
                  name="price"
                  label="Price"
                  type="numder"
                  fullWidth
                  helperText={this.state.errors.price}
                  error={this.state.errors.price ? true : false}
                  onBlur={() => this.handleValidate("price")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  value={this.state.ISBN}
                  onChange={this.handleChange}
                  id="ISBN"
                  name="ISBN"
                  label="ISBN"
                  type="numder"
                  fullWidth
                  disabled={this.state.isInEditMode}
                  helperText={this.state.errors.ISBN}
                  error={this.state.errors.ISBN ? true : false}
                  onBlur={() => this.handleValidate("ISBN")}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="publicationDate"
                  name="publicationDate"
                  label="Publication Date"
                  type="date"
                  value={this.state.publicationDate}
                  onChange={this.handleChange}
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  helperText={this.state.errors.publicationDate}
                  error={this.state.errors.publicationDate ? true : false}
                  onBlur={() => this.handleValidate("publicationDate")}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <FormControl
                  style={styles.formControl}
                  fullWidth
                  error={this.state.errors.GenreId ? true : false}
                  onBlur={() => this.handleValidate("GenreId")}
                >
                  <InputLabel htmlFor="genreId">Genre</InputLabel>
                  <Select
                    value={this.state.GenreId}
                    onChange={this.handleChange}
                    inputProps={{ name: "GenreId", id: "genreId" }}
                    fullWidth
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {this.renderGenreOptions()}
                  </Select>
                  <FormHelperText>{this.state.errors.GenreId}</FormHelperText>
                </FormControl>
              </Grid>
            </Grid>
          </form>
        </CardContent>
        <CardActions>
          <Button size="large" color="primary" onClick={this.onSubmit}>
            Save
          </Button>
          <Link to={"/books/"} style={{ textDecoration: "none" }}>
            <Button size="large" color="secondary">
              Back
            </Button>
          </Link>
        </CardActions>
      </Card>
    );
  }
}

export default BookForm;
