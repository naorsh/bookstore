const axios = require("axios");

export const fetchAllBooks = async () => {
  try {
    const response = await axios.get("/api/books/");

      return response.data;
  } catch (error) {
    console.log(error);
    return {};
  }
};

export const fetchBook = async ISBN => {
  try {
    const response = await axios.get(`/api/books/${ISBN}/`);

    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const addBook = async book => {
  try {
    const response = await axios.post(`/api/books/`, {
      ...book
    });

    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const updateBook = async book => {
  try {
      const response = await axios.put(`/api/books/${book.ISBN}/`, {
        ...book
      });

    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const deleteBook = async ISBN => {
  try {
    const response = await axios.delete(`/api/books/${ISBN}/`);

    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const fetchGenres = async () => {
  try {
    const response = await axios.get(`/api/genres/`);

    return response.data;
  } catch (error) {
    console.log(error);
  }
};
