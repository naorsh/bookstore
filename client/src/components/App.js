import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

//  Material Ui
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import "typeface-roboto";

import BookForm from "./BookForm";
import BookList from "./BookList";
import ErrorPage from "./ErrorPage";

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});

const styles = {
  layout: {
    width: "auto",
    marginLeft: 120,
    marginRight: 120
  }
};

class App extends Component {
  render() {
    return <MuiThemeProvider theme={theme}>
        <div>
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" color="inherit">
                Book Store
              </Typography>
            </Toolbar>
          </AppBar>
          <div name="container" style={styles.layout}>
            <BrowserRouter>
              <Switch>
                <Route path="/books/new/" component={BookForm} />
                <Route path="/books/:id/" component={BookForm} />
                <Route path="/books/" component={BookList} />
                <Route path="/error/" component={ErrorPage} />
                <Redirect from="/" to="/books/" />
              </Switch>
            </BrowserRouter>
          </div>
        </div>
      </MuiThemeProvider>;
  }
}

export default App;
