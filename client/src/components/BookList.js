import React, { Component } from "react";
import { Link } from "react-router-dom";

//  Material-ui
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

//  icons
import AddIcon from "@material-ui/icons/Add";

import { fetchAllBooks, deleteBook } from "../api/api";

const styles = {
  fab: {
    position: "fixed",
    bottom: 100,
    right: 100
  }
};
class BookList extends Component {
  state = {
    books: []
  };

  async componentDidMount() {
    const books = await fetchAllBooks();

    this.setState({ books: books });
  }

  onDeleteBook = ISBN => () => {
    deleteBook(ISBN).then(deletedBookInstance => {
      let spliecedBookList = [...this.state.books];
      var bookToDelete = spliecedBookList.find(function(book) {
        return book.ISBN === deletedBookInstance.ISBN;
      });

      const indexOfBook = spliecedBookList.indexOf(bookToDelete);

      spliecedBookList.splice(indexOfBook, 1);

      this.setState({ books: spliecedBookList });
    });
  };

  renderBookList = () => {
    return this.state.books.map(book => {
      return <Card style={{ margin: 20 }} key={book.ISBN}>
          <CardContent>
            <Typography variant="h5" component="h2">
              {book.title}
            </Typography>
            <Typography gutterBottom variant="h6">
              {book.author}
            </Typography>
          <Typography component="p">{book.description}</Typography>
          <Typography component="p">
              <b>Price:</b> {book.price}
            </Typography>
          </CardContent>
          <CardActions>
            <Link to={`/books/${book.ISBN}/`} style={{ textDecoration: "none" }}>
              <Button size="small" color="primary">
                Watch / Edit
              </Button>
            </Link>
            <Button size="small" color="secondary" onClick={this.onDeleteBook(book.ISBN)}>
              Remove
            </Button>
          </CardActions>
        </Card>;
    });
  };

  render() {
    return (
      <div>
        {this.renderBookList()}
        <Link to="/books/new/">
          <Button variant="fab" color="primary" style={styles.fab}>
            <AddIcon />
          </Button>
        </Link>
      </div>
    );
  }
}

export default BookList;
