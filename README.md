# BookStore
 
A CRUD SPA for books.

Using: 
- ExpressJs
- ReactJs 
- Sequelize ORM.
- PostgreSQL

## Build and run instructions

1. Change the file 'config.dist.json' to 'config.json' fand insert the required
data to the empty strings ("") for each field, in order to connect the ORM  to 
your DB (The project built using postgres).

2. Run the following commands: 
```
// install server dependencies
npm install

// install client dependencies
npm install client 

// in order to run the porject on http://localhost:8000/
npm run dev 

// in order to build to client-side bundle
npm run build 

```